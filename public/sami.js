
window.projectVersion = 'master';

(function(root) {

    var bhIndex = null;
    var rootPath = '';
    var treeHtml = '        <ul>                <li data-name="namespace:" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href=".html">[Global Namespace]</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:FileConfigTrueType" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="FileConfigTrueType.html">FileConfigTrueType</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App" class="opened">                    <div style="padding-left:0px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App.html">App</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:App_Controller" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Controller.html">Controller</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="namespace:App_Controller_API" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Controller/API.html">API</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Controller_API_APIDBConnectionController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Controller/API/APIDBConnectionController.html">APIDBConnectionController</a>                    </div>                </li>                            <li data-name="class:App_Controller_API_APIDBTablesController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Controller/API/APIDBTablesController.html">APIDBTablesController</a>                    </div>                </li>                            <li data-name="class:App_Controller_API_APIDBTypeController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Controller/API/APIDBTypeController.html">APIDBTypeController</a>                    </div>                </li>                            <li data-name="class:App_Controller_API_APIFileConfigController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Controller/API/APIFileConfigController.html">APIFileConfigController</a>                    </div>                </li>                            <li data-name="class:App_Controller_API_APIMTypeController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Controller/API/APIMTypeController.html">APIMTypeController</a>                    </div>                </li>                            <li data-name="class:App_Controller_API_APIMapConfigController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Controller/API/APIMapConfigController.html">APIMapConfigController</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Controller_Admin" >                    <div style="padding-left:36px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Controller/Admin.html">Admin</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Controller_Admin_HomepageController" >                    <div style="padding-left:62px" class="hd leaf">                        <a href="App/Controller/Admin/HomepageController.html">HomepageController</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:App_Controller_DBConnectionController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Controller/DBConnectionController.html">DBConnectionController</a>                    </div>                </li>                            <li data-name="class:App_Controller_DBFieldController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Controller/DBFieldController.html">DBFieldController</a>                    </div>                </li>                            <li data-name="class:App_Controller_DBTablesController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Controller/DBTablesController.html">DBTablesController</a>                    </div>                </li>                            <li data-name="class:App_Controller_FileColumnController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Controller/FileColumnController.html">FileColumnController</a>                    </div>                </li>                            <li data-name="class:App_Controller_FileConfigController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Controller/FileConfigController.html">FileConfigController</a>                    </div>                </li>                            <li data-name="class:App_Controller_HomepageController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Controller/HomepageController.html">HomepageController</a>                    </div>                </li>                            <li data-name="class:App_Controller_MTypeController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Controller/MTypeController.html">MTypeController</a>                    </div>                </li>                            <li data-name="class:App_Controller_MapConfigController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Controller/MapConfigController.html">MapConfigController</a>                    </div>                </li>                            <li data-name="class:App_Controller_RegistrationController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Controller/RegistrationController.html">RegistrationController</a>                    </div>                </li>                            <li data-name="class:App_Controller_SecurityController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Controller/SecurityController.html">SecurityController</a>                    </div>                </li>                            <li data-name="class:App_Controller_UserController" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Controller/UserController.html">UserController</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Entity" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Entity.html">Entity</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Entity_DBConnection" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Entity/DBConnection.html">DBConnection</a>                    </div>                </li>                            <li data-name="class:App_Entity_DBField" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Entity/DBField.html">DBField</a>                    </div>                </li>                            <li data-name="class:App_Entity_DBTables" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Entity/DBTables.html">DBTables</a>                    </div>                </li>                            <li data-name="class:App_Entity_DBType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Entity/DBType.html">DBType</a>                    </div>                </li>                            <li data-name="class:App_Entity_FileColumn" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Entity/FileColumn.html">FileColumn</a>                    </div>                </li>                            <li data-name="class:App_Entity_FileConfig" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Entity/FileConfig.html">FileConfig</a>                    </div>                </li>                            <li data-name="class:App_Entity_MType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Entity/MType.html">MType</a>                    </div>                </li>                            <li data-name="class:App_Entity_MapConfig" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Entity/MapConfig.html">MapConfig</a>                    </div>                </li>                            <li data-name="class:App_Entity_User" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Entity/User.html">User</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Form" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Form.html">Form</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Form_DBConnectionType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Form/DBConnectionType.html">DBConnectionType</a>                    </div>                </li>                            <li data-name="class:App_Form_DBFieldType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Form/DBFieldType.html">DBFieldType</a>                    </div>                </li>                            <li data-name="class:App_Form_DBTablesType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Form/DBTablesType.html">DBTablesType</a>                    </div>                </li>                            <li data-name="class:App_Form_FileColumnType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Form/FileColumnType.html">FileColumnType</a>                    </div>                </li>                            <li data-name="class:App_Form_FileConfigType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Form/FileConfigType.html">FileConfigType</a>                    </div>                </li>                            <li data-name="class:App_Form_MTypeType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Form/MTypeType.html">MTypeType</a>                    </div>                </li>                            <li data-name="class:App_Form_MapConfigType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Form/MapConfigType.html">MapConfigType</a>                    </div>                </li>                            <li data-name="class:App_Form_User1Type" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Form/User1Type.html">User1Type</a>                    </div>                </li>                            <li data-name="class:App_Form_UserType" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Form/UserType.html">UserType</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Repository" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Repository.html">Repository</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Repository_DBConnectionRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Repository/DBConnectionRepository.html">DBConnectionRepository</a>                    </div>                </li>                            <li data-name="class:App_Repository_DBFieldRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Repository/DBFieldRepository.html">DBFieldRepository</a>                    </div>                </li>                            <li data-name="class:App_Repository_DBTablesRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Repository/DBTablesRepository.html">DBTablesRepository</a>                    </div>                </li>                            <li data-name="class:App_Repository_DBTypeRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Repository/DBTypeRepository.html">DBTypeRepository</a>                    </div>                </li>                            <li data-name="class:App_Repository_FileColumnRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Repository/FileColumnRepository.html">FileColumnRepository</a>                    </div>                </li>                            <li data-name="class:App_Repository_FileConfigRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Repository/FileConfigRepository.html">FileConfigRepository</a>                    </div>                </li>                            <li data-name="class:App_Repository_MTypeRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Repository/MTypeRepository.html">MTypeRepository</a>                    </div>                </li>                            <li data-name="class:App_Repository_MapConfigRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Repository/MapConfigRepository.html">MapConfigRepository</a>                    </div>                </li>                            <li data-name="class:App_Repository_UserRepository" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Repository/UserRepository.html">UserRepository</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="namespace:App_Utils" class="opened">                    <div style="padding-left:18px" class="hd">                        <span class="glyphicon glyphicon-play"></span><a href="App/Utils.html">Utils</a>                    </div>                    <div class="bd">                                <ul>                <li data-name="class:App_Utils_CPDO" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Utils/CPDO.html">CPDO</a>                    </div>                </li>                            <li data-name="class:App_Utils_Import" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Utils/Import.html">Import</a>                    </div>                </li>                            <li data-name="class:App_Utils_Utils" >                    <div style="padding-left:44px" class="hd leaf">                        <a href="App/Utils/Utils.html">Utils</a>                    </div>                </li>                </ul></div>                </li>                            <li data-name="class:App_Kernel" class="opened">                    <div style="padding-left:26px" class="hd leaf">                        <a href="App/Kernel.html">Kernel</a>                    </div>                </li>                </ul></div>                </li>                </ul>';

    var searchTypeClasses = {
        'Namespace': 'label-default',
        'Class': 'label-info',
        'Interface': 'label-primary',
        'Trait': 'label-success',
        'Method': 'label-danger',
        '_': 'label-warning'
    };

    var searchIndex = [
                    
            {"type": "Namespace", "link": ".html", "name": "", "doc": "Namespace "},{"type": "Namespace", "link": "App.html", "name": "App", "doc": "Namespace App"},{"type": "Namespace", "link": "App/Controller.html", "name": "App\\Controller", "doc": "Namespace App\\Controller"},{"type": "Namespace", "link": "App/Controller/API.html", "name": "App\\Controller\\API", "doc": "Namespace App\\Controller\\API"},{"type": "Namespace", "link": "App/Controller/Admin.html", "name": "App\\Controller\\Admin", "doc": "Namespace App\\Controller\\Admin"},{"type": "Namespace", "link": "App/Entity.html", "name": "App\\Entity", "doc": "Namespace App\\Entity"},{"type": "Namespace", "link": "App/Form.html", "name": "App\\Form", "doc": "Namespace App\\Form"},{"type": "Namespace", "link": "App/Repository.html", "name": "App\\Repository", "doc": "Namespace App\\Repository"},{"type": "Namespace", "link": "App/Utils.html", "name": "App\\Utils", "doc": "Namespace App\\Utils"},
            
            {"type": "Class", "fromName": "App\\Controller\\API", "fromLink": "App/Controller/API.html", "link": "App/Controller/API/APIDBConnectionController.html", "name": "App\\Controller\\API\\APIDBConnectionController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\API\\APIDBConnectionController", "fromLink": "App/Controller/API/APIDBConnectionController.html", "link": "App/Controller/API/APIDBConnectionController.html#method_all", "name": "App\\Controller\\API\\APIDBConnectionController::all", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\API\\APIDBConnectionController", "fromLink": "App/Controller/API/APIDBConnectionController.html", "link": "App/Controller/API/APIDBConnectionController.html#method_loadDB", "name": "App\\Controller\\API\\APIDBConnectionController::loadDB", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\API\\APIDBConnectionController", "fromLink": "App/Controller/API/APIDBConnectionController.html", "link": "App/Controller/API/APIDBConnectionController.html#method_testDB", "name": "App\\Controller\\API\\APIDBConnectionController::testDB", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\API\\APIDBConnectionController", "fromLink": "App/Controller/API/APIDBConnectionController.html", "link": "App/Controller/API/APIDBConnectionController.html#method_new", "name": "App\\Controller\\API\\APIDBConnectionController::new", "doc": "&quot;Create a new DBConnection (exposed as an API)&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller\\API", "fromLink": "App/Controller/API.html", "link": "App/Controller/API/APIDBTablesController.html", "name": "App\\Controller\\API\\APIDBTablesController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\API\\APIDBTablesController", "fromLink": "App/Controller/API/APIDBTablesController.html", "link": "App/Controller/API/APIDBTablesController.html#method_allByDBConf", "name": "App\\Controller\\API\\APIDBTablesController::allByDBConf", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller\\API", "fromLink": "App/Controller/API.html", "link": "App/Controller/API/APIDBTypeController.html", "name": "App\\Controller\\API\\APIDBTypeController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\API\\APIDBTypeController", "fromLink": "App/Controller/API/APIDBTypeController.html", "link": "App/Controller/API/APIDBTypeController.html#method_all", "name": "App\\Controller\\API\\APIDBTypeController::all", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller\\API", "fromLink": "App/Controller/API.html", "link": "App/Controller/API/APIFileConfigController.html", "name": "App\\Controller\\API\\APIFileConfigController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\API\\APIFileConfigController", "fromLink": "App/Controller/API/APIFileConfigController.html", "link": "App/Controller/API/APIFileConfigController.html#method_allByUser", "name": "App\\Controller\\API\\APIFileConfigController::allByUser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\API\\APIFileConfigController", "fromLink": "App/Controller/API/APIFileConfigController.html", "link": "App/Controller/API/APIFileConfigController.html#method_new", "name": "App\\Controller\\API\\APIFileConfigController::new", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\API\\APIFileConfigController", "fromLink": "App/Controller/API/APIFileConfigController.html", "link": "App/Controller/API/APIFileConfigController.html#method_genColsFromFile", "name": "App\\Controller\\API\\APIFileConfigController::genColsFromFile", "doc": "&quot;Called to get FileColumn[] from file&#039;s headers&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\API\\APIFileConfigController", "fromLink": "App/Controller/API/APIFileConfigController.html", "link": "App/Controller/API/APIFileConfigController.html#method_testFileConfig", "name": "App\\Controller\\API\\APIFileConfigController::testFileConfig", "doc": "&quot;Called to get a treated file&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller\\API", "fromLink": "App/Controller/API.html", "link": "App/Controller/API/APIMTypeController.html", "name": "App\\Controller\\API\\APIMTypeController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\API\\APIMTypeController", "fromLink": "App/Controller/API/APIMTypeController.html", "link": "App/Controller/API/APIMTypeController.html#method_all", "name": "App\\Controller\\API\\APIMTypeController::all", "doc": "&quot;Gives all the M Types&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller\\API", "fromLink": "App/Controller/API.html", "link": "App/Controller/API/APIMapConfigController.html", "name": "App\\Controller\\API\\APIMapConfigController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\API\\APIMapConfigController", "fromLink": "App/Controller/API/APIMapConfigController.html", "link": "App/Controller/API/APIMapConfigController.html#method_allByUser", "name": "App\\Controller\\API\\APIMapConfigController::allByUser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\API\\APIMapConfigController", "fromLink": "App/Controller/API/APIMapConfigController.html", "link": "App/Controller/API/APIMapConfigController.html#method_quickImport", "name": "App\\Controller\\API\\APIMapConfigController::quickImport", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller\\Admin", "fromLink": "App/Controller/Admin.html", "link": "App/Controller/Admin/HomepageController.html", "name": "App\\Controller\\Admin\\HomepageController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\Admin\\HomepageController", "fromLink": "App/Controller/Admin/HomepageController.html", "link": "App/Controller/Admin/HomepageController.html#method_index", "name": "App\\Controller\\Admin\\HomepageController::index", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller", "fromLink": "App/Controller.html", "link": "App/Controller/DBConnectionController.html", "name": "App\\Controller\\DBConnectionController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\DBConnectionController", "fromLink": "App/Controller/DBConnectionController.html", "link": "App/Controller/DBConnectionController.html#method_index", "name": "App\\Controller\\DBConnectionController::index", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBConnectionController", "fromLink": "App/Controller/DBConnectionController.html", "link": "App/Controller/DBConnectionController.html#method_new", "name": "App\\Controller\\DBConnectionController::new", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBConnectionController", "fromLink": "App/Controller/DBConnectionController.html", "link": "App/Controller/DBConnectionController.html#method_all", "name": "App\\Controller\\DBConnectionController::all", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBConnectionController", "fromLink": "App/Controller/DBConnectionController.html", "link": "App/Controller/DBConnectionController.html#method_show", "name": "App\\Controller\\DBConnectionController::show", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBConnectionController", "fromLink": "App/Controller/DBConnectionController.html", "link": "App/Controller/DBConnectionController.html#method_edit", "name": "App\\Controller\\DBConnectionController::edit", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBConnectionController", "fromLink": "App/Controller/DBConnectionController.html", "link": "App/Controller/DBConnectionController.html#method_delete", "name": "App\\Controller\\DBConnectionController::delete", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller", "fromLink": "App/Controller.html", "link": "App/Controller/DBFieldController.html", "name": "App\\Controller\\DBFieldController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\DBFieldController", "fromLink": "App/Controller/DBFieldController.html", "link": "App/Controller/DBFieldController.html#method_index", "name": "App\\Controller\\DBFieldController::index", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBFieldController", "fromLink": "App/Controller/DBFieldController.html", "link": "App/Controller/DBFieldController.html#method_new", "name": "App\\Controller\\DBFieldController::new", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBFieldController", "fromLink": "App/Controller/DBFieldController.html", "link": "App/Controller/DBFieldController.html#method_show", "name": "App\\Controller\\DBFieldController::show", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBFieldController", "fromLink": "App/Controller/DBFieldController.html", "link": "App/Controller/DBFieldController.html#method_edit", "name": "App\\Controller\\DBFieldController::edit", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBFieldController", "fromLink": "App/Controller/DBFieldController.html", "link": "App/Controller/DBFieldController.html#method_delete", "name": "App\\Controller\\DBFieldController::delete", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller", "fromLink": "App/Controller.html", "link": "App/Controller/DBTablesController.html", "name": "App\\Controller\\DBTablesController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\DBTablesController", "fromLink": "App/Controller/DBTablesController.html", "link": "App/Controller/DBTablesController.html#method_index", "name": "App\\Controller\\DBTablesController::index", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBTablesController", "fromLink": "App/Controller/DBTablesController.html", "link": "App/Controller/DBTablesController.html#method_new", "name": "App\\Controller\\DBTablesController::new", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBTablesController", "fromLink": "App/Controller/DBTablesController.html", "link": "App/Controller/DBTablesController.html#method_show", "name": "App\\Controller\\DBTablesController::show", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBTablesController", "fromLink": "App/Controller/DBTablesController.html", "link": "App/Controller/DBTablesController.html#method_edit", "name": "App\\Controller\\DBTablesController::edit", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\DBTablesController", "fromLink": "App/Controller/DBTablesController.html", "link": "App/Controller/DBTablesController.html#method_delete", "name": "App\\Controller\\DBTablesController::delete", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller", "fromLink": "App/Controller.html", "link": "App/Controller/FileColumnController.html", "name": "App\\Controller\\FileColumnController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\FileColumnController", "fromLink": "App/Controller/FileColumnController.html", "link": "App/Controller/FileColumnController.html#method_index", "name": "App\\Controller\\FileColumnController::index", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\FileColumnController", "fromLink": "App/Controller/FileColumnController.html", "link": "App/Controller/FileColumnController.html#method_allByFileConfig", "name": "App\\Controller\\FileColumnController::allByFileConfig", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\FileColumnController", "fromLink": "App/Controller/FileColumnController.html", "link": "App/Controller/FileColumnController.html#method_new", "name": "App\\Controller\\FileColumnController::new", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\FileColumnController", "fromLink": "App/Controller/FileColumnController.html", "link": "App/Controller/FileColumnController.html#method_show", "name": "App\\Controller\\FileColumnController::show", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\FileColumnController", "fromLink": "App/Controller/FileColumnController.html", "link": "App/Controller/FileColumnController.html#method_edit", "name": "App\\Controller\\FileColumnController::edit", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\FileColumnController", "fromLink": "App/Controller/FileColumnController.html", "link": "App/Controller/FileColumnController.html#method_delete", "name": "App\\Controller\\FileColumnController::delete", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller", "fromLink": "App/Controller.html", "link": "App/Controller/FileConfigController.html", "name": "App\\Controller\\FileConfigController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\FileConfigController", "fromLink": "App/Controller/FileConfigController.html", "link": "App/Controller/FileConfigController.html#method_index", "name": "App\\Controller\\FileConfigController::index", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\FileConfigController", "fromLink": "App/Controller/FileConfigController.html", "link": "App/Controller/FileConfigController.html#method_new", "name": "App\\Controller\\FileConfigController::new", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\FileConfigController", "fromLink": "App/Controller/FileConfigController.html", "link": "App/Controller/FileConfigController.html#method_show", "name": "App\\Controller\\FileConfigController::show", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\FileConfigController", "fromLink": "App/Controller/FileConfigController.html", "link": "App/Controller/FileConfigController.html#method_edit", "name": "App\\Controller\\FileConfigController::edit", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\FileConfigController", "fromLink": "App/Controller/FileConfigController.html", "link": "App/Controller/FileConfigController.html#method_delete", "name": "App\\Controller\\FileConfigController::delete", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller", "fromLink": "App/Controller.html", "link": "App/Controller/HomepageController.html", "name": "App\\Controller\\HomepageController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\HomepageController", "fromLink": "App/Controller/HomepageController.html", "link": "App/Controller/HomepageController.html#method_index", "name": "App\\Controller\\HomepageController::index", "doc": "&quot;Controller for homepagen displaying tickets for the connected user\nor all tickets if it is &lt;code&gt;ADMIN&lt;\/code&gt;&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller", "fromLink": "App/Controller.html", "link": "App/Controller/MTypeController.html", "name": "App\\Controller\\MTypeController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\MTypeController", "fromLink": "App/Controller/MTypeController.html", "link": "App/Controller/MTypeController.html#method_index", "name": "App\\Controller\\MTypeController::index", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\MTypeController", "fromLink": "App/Controller/MTypeController.html", "link": "App/Controller/MTypeController.html#method_all", "name": "App\\Controller\\MTypeController::all", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\MTypeController", "fromLink": "App/Controller/MTypeController.html", "link": "App/Controller/MTypeController.html#method_new", "name": "App\\Controller\\MTypeController::new", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\MTypeController", "fromLink": "App/Controller/MTypeController.html", "link": "App/Controller/MTypeController.html#method_show", "name": "App\\Controller\\MTypeController::show", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\MTypeController", "fromLink": "App/Controller/MTypeController.html", "link": "App/Controller/MTypeController.html#method_edit", "name": "App\\Controller\\MTypeController::edit", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\MTypeController", "fromLink": "App/Controller/MTypeController.html", "link": "App/Controller/MTypeController.html#method_delete", "name": "App\\Controller\\MTypeController::delete", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller", "fromLink": "App/Controller.html", "link": "App/Controller/MapConfigController.html", "name": "App\\Controller\\MapConfigController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\MapConfigController", "fromLink": "App/Controller/MapConfigController.html", "link": "App/Controller/MapConfigController.html#method_index", "name": "App\\Controller\\MapConfigController::index", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\MapConfigController", "fromLink": "App/Controller/MapConfigController.html", "link": "App/Controller/MapConfigController.html#method_create", "name": "App\\Controller\\MapConfigController::create", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\MapConfigController", "fromLink": "App/Controller/MapConfigController.html", "link": "App/Controller/MapConfigController.html#method_new", "name": "App\\Controller\\MapConfigController::new", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\MapConfigController", "fromLink": "App/Controller/MapConfigController.html", "link": "App/Controller/MapConfigController.html#method_show", "name": "App\\Controller\\MapConfigController::show", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\MapConfigController", "fromLink": "App/Controller/MapConfigController.html", "link": "App/Controller/MapConfigController.html#method_edit", "name": "App\\Controller\\MapConfigController::edit", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\MapConfigController", "fromLink": "App/Controller/MapConfigController.html", "link": "App/Controller/MapConfigController.html#method_delete", "name": "App\\Controller\\MapConfigController::delete", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller", "fromLink": "App/Controller.html", "link": "App/Controller/RegistrationController.html", "name": "App\\Controller\\RegistrationController", "doc": "&quot;Controller for member registration&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\RegistrationController", "fromLink": "App/Controller/RegistrationController.html", "link": "App/Controller/RegistrationController.html#method_registerAction", "name": "App\\Controller\\RegistrationController::registerAction", "doc": "&quot;Action controller for member registration&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller", "fromLink": "App/Controller.html", "link": "App/Controller/SecurityController.html", "name": "App\\Controller\\SecurityController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\SecurityController", "fromLink": "App/Controller/SecurityController.html", "link": "App/Controller/SecurityController.html#method_login", "name": "App\\Controller\\SecurityController::login", "doc": "&quot;Login action, called when a user (anonymous) attempts to log in&quot;"},
            
            {"type": "Class", "fromName": "App\\Controller", "fromLink": "App/Controller.html", "link": "App/Controller/UserController.html", "name": "App\\Controller\\UserController", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Controller\\UserController", "fromLink": "App/Controller/UserController.html", "link": "App/Controller/UserController.html#method_index", "name": "App\\Controller\\UserController::index", "doc": "&quot;User controller to render all users - restricted to &lt;code&gt;ADMIN&lt;\/code&gt;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\UserController", "fromLink": "App/Controller/UserController.html", "link": "App/Controller/UserController.html#method_new", "name": "App\\Controller\\UserController::new", "doc": "&quot;Controller to create a new user&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\UserController", "fromLink": "App/Controller/UserController.html", "link": "App/Controller/UserController.html#method_show", "name": "App\\Controller\\UserController::show", "doc": "&quot;Controller to render a requested user&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\UserController", "fromLink": "App/Controller/UserController.html", "link": "App/Controller/UserController.html#method_edit", "name": "App\\Controller\\UserController::edit", "doc": "&quot;Controller to allow user&#039;s edition - restricted to &lt;code&gt;ADMIN&lt;\/code&gt;&quot;"},
                    {"type": "Method", "fromName": "App\\Controller\\UserController", "fromLink": "App/Controller/UserController.html", "link": "App/Controller/UserController.html#method_delete", "name": "App\\Controller\\UserController::delete", "doc": "&quot;Controller to delete a user - restricted to &lt;code&gt;ADMIN&lt;\/code&gt;&quot;"},
            
            {"type": "Class", "fromName": "App\\Entity", "fromLink": "App/Entity.html", "link": "App/Entity/DBConnection.html", "name": "App\\Entity\\DBConnection", "doc": "&quot;A &lt;code&gt;DBConnection&lt;\/code&gt; represents a connection to a database.&quot;"},
                                                        {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method___construct", "name": "App\\Entity\\DBConnection::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_loadDB", "name": "App\\Entity\\DBConnection::loadDB", "doc": "&quot;Used to open the connection for the current DBConnection&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getDB", "name": "App\\Entity\\DBConnection::getDB", "doc": "&quot;Used to open the connection for the current DBConnection&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getTables", "name": "App\\Entity\\DBConnection::getTables", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getFields", "name": "App\\Entity\\DBConnection::getFields", "doc": "&quot;Gives an array of all the DBFields&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_jsonSerialize", "name": "App\\Entity\\DBConnection::jsonSerialize", "doc": "&quot;&lt;ul&gt;\n&lt;li&gt;Implementation of \\JsonSerializable&lt;\/li&gt;\n&lt;\/ul&gt;\n&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getId", "name": "App\\Entity\\DBConnection::getId", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getHost", "name": "App\\Entity\\DBConnection::getHost", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_setHost", "name": "App\\Entity\\DBConnection::setHost", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getInternalHost", "name": "App\\Entity\\DBConnection::getInternalHost", "doc": "&quot;Gives the internal host to be used when connection to a database&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getName", "name": "App\\Entity\\DBConnection::getName", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_setName", "name": "App\\Entity\\DBConnection::setName", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getUsername", "name": "App\\Entity\\DBConnection::getUsername", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_setUsername", "name": "App\\Entity\\DBConnection::setUsername", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getPassword", "name": "App\\Entity\\DBConnection::getPassword", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_setPassword", "name": "App\\Entity\\DBConnection::setPassword", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getPort", "name": "App\\Entity\\DBConnection::getPort", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_setPort", "name": "App\\Entity\\DBConnection::setPort", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getAppUser", "name": "App\\Entity\\DBConnection::getAppUser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_setAppUser", "name": "App\\Entity\\DBConnection::setAppUser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method___toString", "name": "App\\Entity\\DBConnection::__toString", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getDBTables", "name": "App\\Entity\\DBConnection::getDBTables", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_addDBTable", "name": "App\\Entity\\DBConnection::addDBTable", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_removeDBTable", "name": "App\\Entity\\DBConnection::removeDBTable", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_getDBType", "name": "App\\Entity\\DBConnection::getDBType", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBConnection", "fromLink": "App/Entity/DBConnection.html", "link": "App/Entity/DBConnection.html#method_setDBType", "name": "App\\Entity\\DBConnection::setDBType", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Entity", "fromLink": "App/Entity.html", "link": "App/Entity/DBField.html", "name": "App\\Entity\\DBField", "doc": "&quot;&lt;code&gt;DBFields&lt;\/code&gt; represents a field of a database.&quot;"},
                                                        {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method___construct", "name": "App\\Entity\\DBField::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_jsonSerialize", "name": "App\\Entity\\DBField::jsonSerialize", "doc": "&quot;&lt;ul&gt;\n&lt;li&gt;Implementation of \\JsonSerializable&lt;\/li&gt;\n&lt;\/ul&gt;\n&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_getId", "name": "App\\Entity\\DBField::getId", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_getName", "name": "App\\Entity\\DBField::getName", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_setName", "name": "App\\Entity\\DBField::setName", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_getValue", "name": "App\\Entity\\DBField::getValue", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_setValue", "name": "App\\Entity\\DBField::setValue", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_getIsPK", "name": "App\\Entity\\DBField::getIsPK", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_setIsPK", "name": "App\\Entity\\DBField::setIsPK", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_getRefField", "name": "App\\Entity\\DBField::getRefField", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_setRefField", "name": "App\\Entity\\DBField::setRefField", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_getDbTable", "name": "App\\Entity\\DBField::getDbTable", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_setDbTable", "name": "App\\Entity\\DBField::setDbTable", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_getMapConfigs", "name": "App\\Entity\\DBField::getMapConfigs", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_addMapConfig", "name": "App\\Entity\\DBField::addMapConfig", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_removeMapConfig", "name": "App\\Entity\\DBField::removeMapConfig", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_getMType", "name": "App\\Entity\\DBField::getMType", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBField", "fromLink": "App/Entity/DBField.html", "link": "App/Entity/DBField.html#method_setMType", "name": "App\\Entity\\DBField::setMType", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Entity", "fromLink": "App/Entity.html", "link": "App/Entity/DBTables.html", "name": "App\\Entity\\DBTables", "doc": "&quot;&lt;code&gt;DBTables&lt;\/code&gt; represents a database&#039;s table.&quot;"},
                                                        {"type": "Method", "fromName": "App\\Entity\\DBTables", "fromLink": "App/Entity/DBTables.html", "link": "App/Entity/DBTables.html#method___construct", "name": "App\\Entity\\DBTables::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBTables", "fromLink": "App/Entity/DBTables.html", "link": "App/Entity/DBTables.html#method_jsonSerialize", "name": "App\\Entity\\DBTables::jsonSerialize", "doc": "&quot;&lt;ul&gt;\n&lt;li&gt;Implementation of \\JsonSerializable&lt;\/li&gt;\n&lt;\/ul&gt;\n&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBTables", "fromLink": "App/Entity/DBTables.html", "link": "App/Entity/DBTables.html#method_getId", "name": "App\\Entity\\DBTables::getId", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBTables", "fromLink": "App/Entity/DBTables.html", "link": "App/Entity/DBTables.html#method_getName", "name": "App\\Entity\\DBTables::getName", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBTables", "fromLink": "App/Entity/DBTables.html", "link": "App/Entity/DBTables.html#method_setName", "name": "App\\Entity\\DBTables::setName", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBTables", "fromLink": "App/Entity/DBTables.html", "link": "App/Entity/DBTables.html#method_getDbConnection", "name": "App\\Entity\\DBTables::getDbConnection", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBTables", "fromLink": "App/Entity/DBTables.html", "link": "App/Entity/DBTables.html#method_setDbConnection", "name": "App\\Entity\\DBTables::setDbConnection", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBTables", "fromLink": "App/Entity/DBTables.html", "link": "App/Entity/DBTables.html#method_getDBFields", "name": "App\\Entity\\DBTables::getDBFields", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBTables", "fromLink": "App/Entity/DBTables.html", "link": "App/Entity/DBTables.html#method_addDBField", "name": "App\\Entity\\DBTables::addDBField", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBTables", "fromLink": "App/Entity/DBTables.html", "link": "App/Entity/DBTables.html#method_removeDBField", "name": "App\\Entity\\DBTables::removeDBField", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Entity", "fromLink": "App/Entity.html", "link": "App/Entity/DBType.html", "name": "App\\Entity\\DBType", "doc": "&quot;&lt;code&gt;DBType&lt;\/code&gt; is a type of database connector (ex: &lt;code&gt;mysql&lt;\/code&gt;).&quot;"},
                                                        {"type": "Method", "fromName": "App\\Entity\\DBType", "fromLink": "App/Entity/DBType.html", "link": "App/Entity/DBType.html#method___construct", "name": "App\\Entity\\DBType::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBType", "fromLink": "App/Entity/DBType.html", "link": "App/Entity/DBType.html#method_jsonSerialize", "name": "App\\Entity\\DBType::jsonSerialize", "doc": "&quot;&lt;ul&gt;\n&lt;li&gt;Implementation of \\JsonSerializable&lt;\/li&gt;\n&lt;\/ul&gt;\n&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBType", "fromLink": "App/Entity/DBType.html", "link": "App/Entity/DBType.html#method_getId", "name": "App\\Entity\\DBType::getId", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBType", "fromLink": "App/Entity/DBType.html", "link": "App/Entity/DBType.html#method_getKeyword", "name": "App\\Entity\\DBType::getKeyword", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBType", "fromLink": "App/Entity/DBType.html", "link": "App/Entity/DBType.html#method_setKeyword", "name": "App\\Entity\\DBType::setKeyword", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBType", "fromLink": "App/Entity/DBType.html", "link": "App/Entity/DBType.html#method_getDbConnections", "name": "App\\Entity\\DBType::getDbConnections", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBType", "fromLink": "App/Entity/DBType.html", "link": "App/Entity/DBType.html#method_addDbConnection", "name": "App\\Entity\\DBType::addDbConnection", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\DBType", "fromLink": "App/Entity/DBType.html", "link": "App/Entity/DBType.html#method_removeDbConnection", "name": "App\\Entity\\DBType::removeDbConnection", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Entity", "fromLink": "App/Entity.html", "link": "App/Entity/FileColumn.html", "name": "App\\Entity\\FileColumn", "doc": "&quot;&lt;code&gt;FileColumn&lt;\/code&gt; is the representation of a file column&quot;"},
                                                        {"type": "Method", "fromName": "App\\Entity\\FileColumn", "fromLink": "App/Entity/FileColumn.html", "link": "App/Entity/FileColumn.html#method_guessType", "name": "App\\Entity\\FileColumn::guessType", "doc": "&quot;Try to guess the type based on the title&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileColumn", "fromLink": "App/Entity/FileColumn.html", "link": "App/Entity/FileColumn.html#method_jsonSerialize", "name": "App\\Entity\\FileColumn::jsonSerialize", "doc": "&quot;&lt;ul&gt;\n&lt;li&gt;Implementation of \\JsonSerializable&lt;\/li&gt;\n&lt;\/ul&gt;\n&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileColumn", "fromLink": "App/Entity/FileColumn.html", "link": "App/Entity/FileColumn.html#method_getId", "name": "App\\Entity\\FileColumn::getId", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileColumn", "fromLink": "App/Entity/FileColumn.html", "link": "App/Entity/FileColumn.html#method_getTitle", "name": "App\\Entity\\FileColumn::getTitle", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileColumn", "fromLink": "App/Entity/FileColumn.html", "link": "App/Entity/FileColumn.html#method_setTitle", "name": "App\\Entity\\FileColumn::setTitle", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileColumn", "fromLink": "App/Entity/FileColumn.html", "link": "App/Entity/FileColumn.html#method_getOrderNb", "name": "App\\Entity\\FileColumn::getOrderNb", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileColumn", "fromLink": "App/Entity/FileColumn.html", "link": "App/Entity/FileColumn.html#method_setOrderNb", "name": "App\\Entity\\FileColumn::setOrderNb", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileColumn", "fromLink": "App/Entity/FileColumn.html", "link": "App/Entity/FileColumn.html#method_getMType", "name": "App\\Entity\\FileColumn::getMType", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileColumn", "fromLink": "App/Entity/FileColumn.html", "link": "App/Entity/FileColumn.html#method_setMType", "name": "App\\Entity\\FileColumn::setMType", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileColumn", "fromLink": "App/Entity/FileColumn.html", "link": "App/Entity/FileColumn.html#method_getFileConfig", "name": "App\\Entity\\FileColumn::getFileConfig", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileColumn", "fromLink": "App/Entity/FileColumn.html", "link": "App/Entity/FileColumn.html#method_setFileConfig", "name": "App\\Entity\\FileColumn::setFileConfig", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Entity", "fromLink": "App/Entity.html", "link": "App/Entity/FileConfig.html", "name": "App\\Entity\\FileConfig", "doc": "&quot;&lt;code&gt;FileConfig&lt;\/code&gt; represents the file configuration which can be used and saved.&quot;"},
                                                        {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method___construct", "name": "App\\Entity\\FileConfig::__construct", "doc": "&quot;&lt;ul&gt;\n&lt;li&gt;Controller&lt;\/li&gt;\n&lt;\/ul&gt;\n&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_all", "name": "App\\Entity\\FileConfig::all", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_jsonSerialize", "name": "App\\Entity\\FileConfig::jsonSerialize", "doc": "&quot;&lt;ul&gt;\n&lt;li&gt;Implementation of \\JsonSerializable&lt;\/li&gt;\n&lt;\/ul&gt;\n&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_getId", "name": "App\\Entity\\FileConfig::getId", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_getName", "name": "App\\Entity\\FileConfig::getName", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_setName", "name": "App\\Entity\\FileConfig::setName", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_getHeaders", "name": "App\\Entity\\FileConfig::getHeaders", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_setHeaders", "name": "App\\Entity\\FileConfig::setHeaders", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_getLineSeparator", "name": "App\\Entity\\FileConfig::getLineSeparator", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_setLineSeparator", "name": "App\\Entity\\FileConfig::setLineSeparator", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_getRowSeparator", "name": "App\\Entity\\FileConfig::getRowSeparator", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_setRowSeparator", "name": "App\\Entity\\FileConfig::setRowSeparator", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_getAppUser", "name": "App\\Entity\\FileConfig::getAppUser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_setAppUser", "name": "App\\Entity\\FileConfig::setAppUser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_getFileColumns", "name": "App\\Entity\\FileConfig::getFileColumns", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_addFileColumn", "name": "App\\Entity\\FileConfig::addFileColumn", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_removeFileColumn", "name": "App\\Entity\\FileConfig::removeFileColumn", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_getMapConfigs", "name": "App\\Entity\\FileConfig::getMapConfigs", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_addMapConfig", "name": "App\\Entity\\FileConfig::addMapConfig", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\FileConfig", "fromLink": "App/Entity/FileConfig.html", "link": "App/Entity/FileConfig.html#method_removeMapConfig", "name": "App\\Entity\\FileConfig::removeMapConfig", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Entity", "fromLink": "App/Entity.html", "link": "App/Entity/MType.html", "name": "App\\Entity\\MType", "doc": "&quot;&lt;code&gt;MType&lt;\/code&gt; is a type attributed to a &lt;code&gt;FileColumn&lt;\/code&gt; or a &lt;code&gt;DBField&lt;\/code&gt;.&quot;"},
                                                        {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method___construct", "name": "App\\Entity\\MType::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method_getTypeFromKeyword", "name": "App\\Entity\\MType::getTypeFromKeyword", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method_all", "name": "App\\Entity\\MType::all", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method_jsonSerialize", "name": "App\\Entity\\MType::jsonSerialize", "doc": "&quot;&lt;ul&gt;\n&lt;li&gt;Implementation of \\JsonSerializable&lt;\/li&gt;\n&lt;\/ul&gt;\n&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method_getId", "name": "App\\Entity\\MType::getId", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method_getLabel", "name": "App\\Entity\\MType::getLabel", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method_setLabel", "name": "App\\Entity\\MType::setLabel", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method_getFileColumns", "name": "App\\Entity\\MType::getFileColumns", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method_addFileColumn", "name": "App\\Entity\\MType::addFileColumn", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method_removeFileColumn", "name": "App\\Entity\\MType::removeFileColumn", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method_getDBFields", "name": "App\\Entity\\MType::getDBFields", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method_addDBField", "name": "App\\Entity\\MType::addDBField", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MType", "fromLink": "App/Entity/MType.html", "link": "App/Entity/MType.html#method_removeDBField", "name": "App\\Entity\\MType::removeDBField", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Entity", "fromLink": "App/Entity.html", "link": "App/Entity/MapConfig.html", "name": "App\\Entity\\MapConfig", "doc": "&quot;&lt;code&gt;MapConfig&lt;\/code&gt; represents a mapping configuration which can be used and saved.&quot;"},
                                                        {"type": "Method", "fromName": "App\\Entity\\MapConfig", "fromLink": "App/Entity/MapConfig.html", "link": "App/Entity/MapConfig.html#method___construct", "name": "App\\Entity\\MapConfig::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MapConfig", "fromLink": "App/Entity/MapConfig.html", "link": "App/Entity/MapConfig.html#method_jsonSerialize", "name": "App\\Entity\\MapConfig::jsonSerialize", "doc": "&quot;&lt;ul&gt;\n&lt;li&gt;Implementation of \\JsonSerializable&lt;\/li&gt;\n&lt;\/ul&gt;\n&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MapConfig", "fromLink": "App/Entity/MapConfig.html", "link": "App/Entity/MapConfig.html#method_getId", "name": "App\\Entity\\MapConfig::getId", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MapConfig", "fromLink": "App/Entity/MapConfig.html", "link": "App/Entity/MapConfig.html#method_getName", "name": "App\\Entity\\MapConfig::getName", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MapConfig", "fromLink": "App/Entity/MapConfig.html", "link": "App/Entity/MapConfig.html#method_setName", "name": "App\\Entity\\MapConfig::setName", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MapConfig", "fromLink": "App/Entity/MapConfig.html", "link": "App/Entity/MapConfig.html#method_getFileConfig", "name": "App\\Entity\\MapConfig::getFileConfig", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MapConfig", "fromLink": "App/Entity/MapConfig.html", "link": "App/Entity/MapConfig.html#method_setFileConfig", "name": "App\\Entity\\MapConfig::setFileConfig", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MapConfig", "fromLink": "App/Entity/MapConfig.html", "link": "App/Entity/MapConfig.html#method_getDbFields", "name": "App\\Entity\\MapConfig::getDbFields", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MapConfig", "fromLink": "App/Entity/MapConfig.html", "link": "App/Entity/MapConfig.html#method_addDbField", "name": "App\\Entity\\MapConfig::addDbField", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MapConfig", "fromLink": "App/Entity/MapConfig.html", "link": "App/Entity/MapConfig.html#method_removeDbField", "name": "App\\Entity\\MapConfig::removeDbField", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MapConfig", "fromLink": "App/Entity/MapConfig.html", "link": "App/Entity/MapConfig.html#method_getCreatedBy", "name": "App\\Entity\\MapConfig::getCreatedBy", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\MapConfig", "fromLink": "App/Entity/MapConfig.html", "link": "App/Entity/MapConfig.html#method_setCreatedBy", "name": "App\\Entity\\MapConfig::setCreatedBy", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Entity", "fromLink": "App/Entity.html", "link": "App/Entity/User.html", "name": "App\\Entity\\User", "doc": "&quot;&lt;code&gt;User&lt;\/code&gt; is a user who can log in and log out.&quot;"},
                                                        {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method___construct", "name": "App\\Entity\\User::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_getUsername", "name": "App\\Entity\\User::getUsername", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_getSalt", "name": "App\\Entity\\User::getSalt", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_getPassword", "name": "App\\Entity\\User::getPassword", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_setPassword", "name": "App\\Entity\\User::setPassword", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_getRoles", "name": "App\\Entity\\User::getRoles", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_addRole", "name": "App\\Entity\\User::addRole", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_eraseCredentials", "name": "App\\Entity\\User::eraseCredentials", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_serialize", "name": "App\\Entity\\User::serialize", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_unserialize", "name": "App\\Entity\\User::unserialize", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_getId", "name": "App\\Entity\\User::getId", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_getlogin", "name": "App\\Entity\\User::getlogin", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_getPlainPassword", "name": "App\\Entity\\User::getPlainPassword", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_getIsActive", "name": "App\\Entity\\User::getIsActive", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_setId", "name": "App\\Entity\\User::setId", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_setlogin", "name": "App\\Entity\\User::setlogin", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_setPlainPassword", "name": "App\\Entity\\User::setPlainPassword", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_setIsActive", "name": "App\\Entity\\User::setIsActive", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_getDBConnections", "name": "App\\Entity\\User::getDBConnections", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_addDBConnection", "name": "App\\Entity\\User::addDBConnection", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_removeDBConnection", "name": "App\\Entity\\User::removeDBConnection", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_getFileConfigs", "name": "App\\Entity\\User::getFileConfigs", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_addFileConfig", "name": "App\\Entity\\User::addFileConfig", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_removeFileConfig", "name": "App\\Entity\\User::removeFileConfig", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_getMapConfigs", "name": "App\\Entity\\User::getMapConfigs", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_addMapConfig", "name": "App\\Entity\\User::addMapConfig", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Entity\\User", "fromLink": "App/Entity/User.html", "link": "App/Entity/User.html#method_removeMapConfig", "name": "App\\Entity\\User::removeMapConfig", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Form", "fromLink": "App/Form.html", "link": "App/Form/DBConnectionType.html", "name": "App\\Form\\DBConnectionType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Form\\DBConnectionType", "fromLink": "App/Form/DBConnectionType.html", "link": "App/Form/DBConnectionType.html#method_buildForm", "name": "App\\Form\\DBConnectionType::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Form\\DBConnectionType", "fromLink": "App/Form/DBConnectionType.html", "link": "App/Form/DBConnectionType.html#method_configureOptions", "name": "App\\Form\\DBConnectionType::configureOptions", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Form", "fromLink": "App/Form.html", "link": "App/Form/DBFieldType.html", "name": "App\\Form\\DBFieldType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Form\\DBFieldType", "fromLink": "App/Form/DBFieldType.html", "link": "App/Form/DBFieldType.html#method_buildForm", "name": "App\\Form\\DBFieldType::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Form\\DBFieldType", "fromLink": "App/Form/DBFieldType.html", "link": "App/Form/DBFieldType.html#method_configureOptions", "name": "App\\Form\\DBFieldType::configureOptions", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Form", "fromLink": "App/Form.html", "link": "App/Form/DBTablesType.html", "name": "App\\Form\\DBTablesType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Form\\DBTablesType", "fromLink": "App/Form/DBTablesType.html", "link": "App/Form/DBTablesType.html#method_buildForm", "name": "App\\Form\\DBTablesType::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Form\\DBTablesType", "fromLink": "App/Form/DBTablesType.html", "link": "App/Form/DBTablesType.html#method_configureOptions", "name": "App\\Form\\DBTablesType::configureOptions", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Form", "fromLink": "App/Form.html", "link": "App/Form/FileColumnType.html", "name": "App\\Form\\FileColumnType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Form\\FileColumnType", "fromLink": "App/Form/FileColumnType.html", "link": "App/Form/FileColumnType.html#method_buildForm", "name": "App\\Form\\FileColumnType::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Form\\FileColumnType", "fromLink": "App/Form/FileColumnType.html", "link": "App/Form/FileColumnType.html#method_configureOptions", "name": "App\\Form\\FileColumnType::configureOptions", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Form", "fromLink": "App/Form.html", "link": "App/Form/FileConfigType.html", "name": "App\\Form\\FileConfigType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Form\\FileConfigType", "fromLink": "App/Form/FileConfigType.html", "link": "App/Form/FileConfigType.html#method_buildForm", "name": "App\\Form\\FileConfigType::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Form\\FileConfigType", "fromLink": "App/Form/FileConfigType.html", "link": "App/Form/FileConfigType.html#method_configureOptions", "name": "App\\Form\\FileConfigType::configureOptions", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Form", "fromLink": "App/Form.html", "link": "App/Form/MTypeType.html", "name": "App\\Form\\MTypeType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Form\\MTypeType", "fromLink": "App/Form/MTypeType.html", "link": "App/Form/MTypeType.html#method_buildForm", "name": "App\\Form\\MTypeType::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Form\\MTypeType", "fromLink": "App/Form/MTypeType.html", "link": "App/Form/MTypeType.html#method_configureOptions", "name": "App\\Form\\MTypeType::configureOptions", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Form", "fromLink": "App/Form.html", "link": "App/Form/MapConfigType.html", "name": "App\\Form\\MapConfigType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Form\\MapConfigType", "fromLink": "App/Form/MapConfigType.html", "link": "App/Form/MapConfigType.html#method_buildForm", "name": "App\\Form\\MapConfigType::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Form\\MapConfigType", "fromLink": "App/Form/MapConfigType.html", "link": "App/Form/MapConfigType.html#method_configureOptions", "name": "App\\Form\\MapConfigType::configureOptions", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Form", "fromLink": "App/Form.html", "link": "App/Form/User1Type.html", "name": "App\\Form\\User1Type", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Form\\User1Type", "fromLink": "App/Form/User1Type.html", "link": "App/Form/User1Type.html#method_buildForm", "name": "App\\Form\\User1Type::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Form\\User1Type", "fromLink": "App/Form/User1Type.html", "link": "App/Form/User1Type.html#method_configureOptions", "name": "App\\Form\\User1Type::configureOptions", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Form", "fromLink": "App/Form.html", "link": "App/Form/UserType.html", "name": "App\\Form\\UserType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Form\\UserType", "fromLink": "App/Form/UserType.html", "link": "App/Form/UserType.html#method_buildForm", "name": "App\\Form\\UserType::buildForm", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App", "fromLink": "App.html", "link": "App/Kernel.html", "name": "App\\Kernel", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Kernel", "fromLink": "App/Kernel.html", "link": "App/Kernel.html#method_getCacheDir", "name": "App\\Kernel::getCacheDir", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Kernel", "fromLink": "App/Kernel.html", "link": "App/Kernel.html#method_getLogDir", "name": "App\\Kernel::getLogDir", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Kernel", "fromLink": "App/Kernel.html", "link": "App/Kernel.html#method_registerBundles", "name": "App\\Kernel::registerBundles", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Kernel", "fromLink": "App/Kernel.html", "link": "App/Kernel.html#method_configureContainer", "name": "App\\Kernel::configureContainer", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Kernel", "fromLink": "App/Kernel.html", "link": "App/Kernel.html#method_configureRoutes", "name": "App\\Kernel::configureRoutes", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Repository", "fromLink": "App/Repository.html", "link": "App/Repository/DBConnectionRepository.html", "name": "App\\Repository\\DBConnectionRepository", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Repository\\DBConnectionRepository", "fromLink": "App/Repository/DBConnectionRepository.html", "link": "App/Repository/DBConnectionRepository.html#method___construct", "name": "App\\Repository\\DBConnectionRepository::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\DBConnectionRepository", "fromLink": "App/Repository/DBConnectionRepository.html", "link": "App/Repository/DBConnectionRepository.html#method_findByAppUser", "name": "App\\Repository\\DBConnectionRepository::findByAppUser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\DBConnectionRepository", "fromLink": "App/Repository/DBConnectionRepository.html", "link": "App/Repository/DBConnectionRepository.html#method_findOne", "name": "App\\Repository\\DBConnectionRepository::findOne", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\DBConnectionRepository", "fromLink": "App/Repository/DBConnectionRepository.html", "link": "App/Repository/DBConnectionRepository.html#method_find", "name": "App\\Repository\\DBConnectionRepository::find", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\DBConnectionRepository", "fromLink": "App/Repository/DBConnectionRepository.html", "link": "App/Repository/DBConnectionRepository.html#method_findOneBy", "name": "App\\Repository\\DBConnectionRepository::findOneBy", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Repository", "fromLink": "App/Repository.html", "link": "App/Repository/DBFieldRepository.html", "name": "App\\Repository\\DBFieldRepository", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Repository\\DBFieldRepository", "fromLink": "App/Repository/DBFieldRepository.html", "link": "App/Repository/DBFieldRepository.html#method___construct", "name": "App\\Repository\\DBFieldRepository::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\DBFieldRepository", "fromLink": "App/Repository/DBFieldRepository.html", "link": "App/Repository/DBFieldRepository.html#method_find", "name": "App\\Repository\\DBFieldRepository::find", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\DBFieldRepository", "fromLink": "App/Repository/DBFieldRepository.html", "link": "App/Repository/DBFieldRepository.html#method_findOneBy", "name": "App\\Repository\\DBFieldRepository::findOneBy", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Repository", "fromLink": "App/Repository.html", "link": "App/Repository/DBTablesRepository.html", "name": "App\\Repository\\DBTablesRepository", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Repository\\DBTablesRepository", "fromLink": "App/Repository/DBTablesRepository.html", "link": "App/Repository/DBTablesRepository.html#method___construct", "name": "App\\Repository\\DBTablesRepository::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\DBTablesRepository", "fromLink": "App/Repository/DBTablesRepository.html", "link": "App/Repository/DBTablesRepository.html#method_findByDBConf", "name": "App\\Repository\\DBTablesRepository::findByDBConf", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\DBTablesRepository", "fromLink": "App/Repository/DBTablesRepository.html", "link": "App/Repository/DBTablesRepository.html#method_find", "name": "App\\Repository\\DBTablesRepository::find", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\DBTablesRepository", "fromLink": "App/Repository/DBTablesRepository.html", "link": "App/Repository/DBTablesRepository.html#method_findOneBy", "name": "App\\Repository\\DBTablesRepository::findOneBy", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Repository", "fromLink": "App/Repository.html", "link": "App/Repository/DBTypeRepository.html", "name": "App\\Repository\\DBTypeRepository", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Repository\\DBTypeRepository", "fromLink": "App/Repository/DBTypeRepository.html", "link": "App/Repository/DBTypeRepository.html#method___construct", "name": "App\\Repository\\DBTypeRepository::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\DBTypeRepository", "fromLink": "App/Repository/DBTypeRepository.html", "link": "App/Repository/DBTypeRepository.html#method_find", "name": "App\\Repository\\DBTypeRepository::find", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\DBTypeRepository", "fromLink": "App/Repository/DBTypeRepository.html", "link": "App/Repository/DBTypeRepository.html#method_findOneBy", "name": "App\\Repository\\DBTypeRepository::findOneBy", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Repository", "fromLink": "App/Repository.html", "link": "App/Repository/FileColumnRepository.html", "name": "App\\Repository\\FileColumnRepository", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Repository\\FileColumnRepository", "fromLink": "App/Repository/FileColumnRepository.html", "link": "App/Repository/FileColumnRepository.html#method___construct", "name": "App\\Repository\\FileColumnRepository::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\FileColumnRepository", "fromLink": "App/Repository/FileColumnRepository.html", "link": "App/Repository/FileColumnRepository.html#method_findAllByFileConfig", "name": "App\\Repository\\FileColumnRepository::findAllByFileConfig", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\FileColumnRepository", "fromLink": "App/Repository/FileColumnRepository.html", "link": "App/Repository/FileColumnRepository.html#method_find", "name": "App\\Repository\\FileColumnRepository::find", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\FileColumnRepository", "fromLink": "App/Repository/FileColumnRepository.html", "link": "App/Repository/FileColumnRepository.html#method_findOneBy", "name": "App\\Repository\\FileColumnRepository::findOneBy", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Repository", "fromLink": "App/Repository.html", "link": "App/Repository/FileConfigRepository.html", "name": "App\\Repository\\FileConfigRepository", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Repository\\FileConfigRepository", "fromLink": "App/Repository/FileConfigRepository.html", "link": "App/Repository/FileConfigRepository.html#method___construct", "name": "App\\Repository\\FileConfigRepository::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\FileConfigRepository", "fromLink": "App/Repository/FileConfigRepository.html", "link": "App/Repository/FileConfigRepository.html#method_findAllByUser", "name": "App\\Repository\\FileConfigRepository::findAllByUser", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\FileConfigRepository", "fromLink": "App/Repository/FileConfigRepository.html", "link": "App/Repository/FileConfigRepository.html#method_find", "name": "App\\Repository\\FileConfigRepository::find", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\FileConfigRepository", "fromLink": "App/Repository/FileConfigRepository.html", "link": "App/Repository/FileConfigRepository.html#method_findOneBy", "name": "App\\Repository\\FileConfigRepository::findOneBy", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Repository", "fromLink": "App/Repository.html", "link": "App/Repository/MTypeRepository.html", "name": "App\\Repository\\MTypeRepository", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Repository\\MTypeRepository", "fromLink": "App/Repository/MTypeRepository.html", "link": "App/Repository/MTypeRepository.html#method___construct", "name": "App\\Repository\\MTypeRepository::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\MTypeRepository", "fromLink": "App/Repository/MTypeRepository.html", "link": "App/Repository/MTypeRepository.html#method_findOneByID", "name": "App\\Repository\\MTypeRepository::findOneByID", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\MTypeRepository", "fromLink": "App/Repository/MTypeRepository.html", "link": "App/Repository/MTypeRepository.html#method_find", "name": "App\\Repository\\MTypeRepository::find", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\MTypeRepository", "fromLink": "App/Repository/MTypeRepository.html", "link": "App/Repository/MTypeRepository.html#method_findOneBy", "name": "App\\Repository\\MTypeRepository::findOneBy", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Repository", "fromLink": "App/Repository.html", "link": "App/Repository/MapConfigRepository.html", "name": "App\\Repository\\MapConfigRepository", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Repository\\MapConfigRepository", "fromLink": "App/Repository/MapConfigRepository.html", "link": "App/Repository/MapConfigRepository.html#method___construct", "name": "App\\Repository\\MapConfigRepository::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\MapConfigRepository", "fromLink": "App/Repository/MapConfigRepository.html", "link": "App/Repository/MapConfigRepository.html#method_findAllByUser", "name": "App\\Repository\\MapConfigRepository::findAllByUser", "doc": "&quot;Gives all MapConfigs for the given user&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\MapConfigRepository", "fromLink": "App/Repository/MapConfigRepository.html", "link": "App/Repository/MapConfigRepository.html#method_find", "name": "App\\Repository\\MapConfigRepository::find", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\MapConfigRepository", "fromLink": "App/Repository/MapConfigRepository.html", "link": "App/Repository/MapConfigRepository.html#method_findOneBy", "name": "App\\Repository\\MapConfigRepository::findOneBy", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Repository", "fromLink": "App/Repository.html", "link": "App/Repository/UserRepository.html", "name": "App\\Repository\\UserRepository", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Repository\\UserRepository", "fromLink": "App/Repository/UserRepository.html", "link": "App/Repository/UserRepository.html#method___construct", "name": "App\\Repository\\UserRepository::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\UserRepository", "fromLink": "App/Repository/UserRepository.html", "link": "App/Repository/UserRepository.html#method_find", "name": "App\\Repository\\UserRepository::find", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Repository\\UserRepository", "fromLink": "App/Repository/UserRepository.html", "link": "App/Repository/UserRepository.html#method_findOneBy", "name": "App\\Repository\\UserRepository::findOneBy", "doc": "&quot;&quot;"},
            
            {"type": "Class", "fromName": "App\\Utils", "fromLink": "App/Utils.html", "link": "App/Utils/CPDO.html", "name": "App\\Utils\\CPDO", "doc": "&quot;Custom PDO that is serializabled&quot;"},
                                                        {"type": "Method", "fromName": "App\\Utils\\CPDO", "fromLink": "App/Utils/CPDO.html", "link": "App/Utils/CPDO.html#method___construct", "name": "App\\Utils\\CPDO::__construct", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Utils\\CPDO", "fromLink": "App/Utils/CPDO.html", "link": "App/Utils/CPDO.html#method_serialize", "name": "App\\Utils\\CPDO::serialize", "doc": "&quot;Serialize object&quot;"},
                    {"type": "Method", "fromName": "App\\Utils\\CPDO", "fromLink": "App/Utils/CPDO.html", "link": "App/Utils/CPDO.html#method_unserialize", "name": "App\\Utils\\CPDO::unserialize", "doc": "&quot;Unserialize object&quot;"},
                    {"type": "Method", "fromName": "App\\Utils\\CPDO", "fromLink": "App/Utils/CPDO.html", "link": "App/Utils/CPDO.html#method_getPdo", "name": "App\\Utils\\CPDO::getPdo", "doc": "&quot;Get the value of pdo&quot;"},
                    {"type": "Method", "fromName": "App\\Utils\\CPDO", "fromLink": "App/Utils/CPDO.html", "link": "App/Utils/CPDO.html#method_setPdo", "name": "App\\Utils\\CPDO::setPdo", "doc": "&quot;Set the value of pdo&quot;"},
            
            {"type": "Class", "fromName": "App\\Utils", "fromLink": "App/Utils.html", "link": "App/Utils/Import.html", "name": "App\\Utils\\Import", "doc": "&quot;class used for imports&quot;"},
                                                        {"type": "Method", "fromName": "App\\Utils\\Import", "fromLink": "App/Utils/Import.html", "link": "App/Utils/Import.html#method_treatFile", "name": "App\\Utils\\Import::treatFile", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "App\\Utils\\Import", "fromLink": "App/Utils/Import.html", "link": "App/Utils/Import.html#method_treatFileCSV", "name": "App\\Utils\\Import::treatFileCSV", "doc": "&quot;Treat a CSV file&quot;"},
                    {"type": "Method", "fromName": "App\\Utils\\Import", "fromLink": "App/Utils/Import.html", "link": "App/Utils/Import.html#method_quicky", "name": "App\\Utils\\Import::quicky", "doc": "&quot;Quick import, no logs or whatsoever&quot;"},
                    {"type": "Method", "fromName": "App\\Utils\\Import", "fromLink": "App/Utils/Import.html", "link": "App/Utils/Import.html#method_getBetterLinks", "name": "App\\Utils\\Import::getBetterLinks", "doc": "&quot;Gives an array easier to manipulate in this form:&lt;\/p&gt;\n\n&lt;pre&gt;&lt;code&gt;[\n    TABLE_NAME =&amp;gt; [\n                    [FILECOL =&amp;gt; FileCol(Obj),\n                    DBFIELD =&amp;gt; FieldName(String)],\n                ]\n]\n&lt;\/code&gt;&lt;\/pre&gt;\n&quot;"},
            
            {"type": "Class", "fromName": "App\\Utils", "fromLink": "App/Utils.html", "link": "App/Utils/Utils.html", "name": "App\\Utils\\Utils", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "App\\Utils\\Utils", "fromLink": "App/Utils/Utils.html", "link": "App/Utils/Utils.html#method_indexOfObjLbl", "name": "App\\Utils\\Utils::indexOfObjLbl", "doc": "&quot;Gives the array index of an object but requires to the objects to have the &lt;code&gt;-&amp;gt;getTitle()&lt;\/code&gt; method&quot;"},
            
            {"type": "Class",  "link": "FileConfigTrueType.html", "name": "FileConfigTrueType", "doc": "&quot;&quot;"},
                                                        {"type": "Method", "fromName": "FileConfigTrueType", "fromLink": "FileConfigTrueType.html", "link": "FileConfigTrueType.html#method_buildForm", "name": "FileConfigTrueType::buildForm", "doc": "&quot;&quot;"},
                    {"type": "Method", "fromName": "FileConfigTrueType", "fromLink": "FileConfigTrueType.html", "link": "FileConfigTrueType.html#method_configureOptions", "name": "FileConfigTrueType::configureOptions", "doc": "&quot;&quot;"},
            
            
                                        // Fix trailing commas in the index
        {}
    ];

    /** Tokenizes strings by namespaces and functions */
    function tokenizer(term) {
        if (!term) {
            return [];
        }

        var tokens = [term];
        var meth = term.indexOf('::');

        // Split tokens into methods if "::" is found.
        if (meth > -1) {
            tokens.push(term.substr(meth + 2));
            term = term.substr(0, meth - 2);
        }

        // Split by namespace or fake namespace.
        if (term.indexOf('\\') > -1) {
            tokens = tokens.concat(term.split('\\'));
        } else if (term.indexOf('_') > 0) {
            tokens = tokens.concat(term.split('_'));
        }

        // Merge in splitting the string by case and return
        tokens = tokens.concat(term.match(/(([A-Z]?[^A-Z]*)|([a-z]?[^a-z]*))/g).slice(0,-1));

        return tokens;
    };

    root.Sami = {
        /**
         * Cleans the provided term. If no term is provided, then one is
         * grabbed from the query string "search" parameter.
         */
        cleanSearchTerm: function(term) {
            // Grab from the query string
            if (typeof term === 'undefined') {
                var name = 'search';
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)");
                var results = regex.exec(location.search);
                if (results === null) {
                    return null;
                }
                term = decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            return term.replace(/<(?:.|\n)*?>/gm, '');
        },

        /** Searches through the index for a given term */
        search: function(term) {
            // Create a new search index if needed
            if (!bhIndex) {
                bhIndex = new Bloodhound({
                    limit: 500,
                    local: searchIndex,
                    datumTokenizer: function (d) {
                        return tokenizer(d.name);
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace
                });
                bhIndex.initialize();
            }

            results = [];
            bhIndex.get(term, function(matches) {
                results = matches;
            });

            if (!rootPath) {
                return results;
            }

            // Fix the element links based on the current page depth.
            return $.map(results, function(ele) {
                if (ele.link.indexOf('..') > -1) {
                    return ele;
                }
                ele.link = rootPath + ele.link;
                if (ele.fromLink) {
                    ele.fromLink = rootPath + ele.fromLink;
                }
                return ele;
            });
        },

        /** Get a search class for a specific type */
        getSearchClass: function(type) {
            return searchTypeClasses[type] || searchTypeClasses['_'];
        },

        /** Add the left-nav tree to the site */
        injectApiTree: function(ele) {
            ele.html(treeHtml);
        }
    };

    $(function() {
        // Modify the HTML to work correctly based on the current depth
        rootPath = $('body').attr('data-root-path');
        treeHtml = treeHtml.replace(/href="/g, 'href="' + rootPath);
        Sami.injectApiTree($('#api-tree'));
    });

    return root.Sami;
})(window);

$(function() {

    // Enable the version switcher
    $('#version-switcher').change(function() {
        window.location = $(this).val()
    });

    
        // Toggle left-nav divs on click
        $('#api-tree .hd span').click(function() {
            $(this).parent().parent().toggleClass('opened');
        });

        // Expand the parent namespaces of the current page.
        var expected = $('body').attr('data-name');

        if (expected) {
            // Open the currently selected node and its parents.
            var container = $('#api-tree');
            var node = $('#api-tree li[data-name="' + expected + '"]');
            // Node might not be found when simulating namespaces
            if (node.length > 0) {
                node.addClass('active').addClass('opened');
                node.parents('li').addClass('opened');
                var scrollPos = node.offset().top - container.offset().top + container.scrollTop();
                // Position the item nearer to the top of the screen.
                scrollPos -= 200;
                container.scrollTop(scrollPos);
            }
        }

    
    
        var form = $('#search-form .typeahead');
        form.typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'search',
            displayKey: 'name',
            source: function (q, cb) {
                cb(Sami.search(q));
            }
        });

        // The selection is direct-linked when the user selects a suggestion.
        form.on('typeahead:selected', function(e, suggestion) {
            window.location = suggestion.link;
        });

        // The form is submitted when the user hits enter.
        form.keypress(function (e) {
            if (e.which == 13) {
                $('#search-form').submit();
                return true;
            }
        });

    
});


